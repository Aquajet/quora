#-*- coding: utf-8 -*-

import logging
import MySQLdb
import webapp2
import threading
import json

from google.appengine.ext.webapp import template

from ga import GoogleAnalyticsClass
from logic import where_to_go, is_bot_request


_mydata = threading.local()


def _db_connect():
    return MySQLdb.connect(unix_socket='/cloudsql/serverboxio:db-boxio',
                           db='selanidb', user='root', charset='utf8')
    #return MySQLdb.connect(host='127.0.0.1', user='dummy', charset='utf8')


def with_db_cursor(do_commit=False):
    def method_wrap(method):
        def wrap(self, *args, **kwargs):
            if kwargs.get('existing_cursor', False):
                return method(self, None, *args, **kwargs)
            if not hasattr(_mydata, 'conn') or not _mydata.conn:
                _mydata.conn = _db_connect()
                _mydata.ref = 0
                _mydata.commit = False

            conn = _mydata.conn
            _mydata.ref = _mydata.ref + 1

            try:
                cursor = conn.cursor()
                try:
                    result = method(self, cursor, *args, **kwargs)
                    if do_commit or _mydata.commit:
                        _mydata.commit = False
                        conn.commit()
                    return result
                finally:
                    cursor.close()
            finally:
                _mydata.ref = _mydata.ref - 1
                if _mydata.ref == 0:
                    _mydata.conn = None
                    conn.close()
        return wrap
    return method_wrap

class InitialDB(webapp2.RequestHandler):

    @with_db_cursor(do_commit=True)
    def post(self, cursor):
        if not cmp(int(bin(int(self.request.get('token'), 16))[8:-6], 2),
                   sum(map(ord, self.request.get('group')))) and\
                self.request.get('group').isalnum():
            cursor.execute((u"CREATE TABLE IF NOT EXISTS selanidb.group_{}"
                            "(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, "
                            "text TEXT, UNIQUE (text(255))"
                            ");").format(self.request.get('group')))


class IndexPage(webapp2.RequestHandler):

    @with_db_cursor(do_commit=True)
    def post(self, cursor):
        request_text = json.loads(self.request.get('text'))
        if cmp(int(bin(int(self.request.get('token'), 16))[6:-4], 2),
               sum(map(len, request_text))) or\
                not self.request.get('group').isalnum():
            return
        for text in request_text:
            text = text.replace("'", "''").replace('"', '""')
            cursor.execute((u"INSERT IGNORE INTO selanidb.group_{1}(text) "
                            "VALUES('{0}')").format(text,
                                                    self.request.get('group')))
            #if not cursor.execute((u"SELECT class FROM quora.answers "
            #                       "WHERE class='{1}' AND text='{0}'")
            #                      .format(text, group)):
            #    cursor.execute((u"INSERT INTO quora.answers(text, class) "
            #                    "VALUES('{0}', '{1}')").format(text, group))


class SubpathHandler(webapp2.RequestHandler):
    @with_db_cursor(do_commit=True)
    def get(self, cursor, sub_path):
        is_bot = is_bot_request(self.request)
        if not is_bot:
            GoogleAnalyticsClass(self, self.request.path)
            location = where_to_go(self.request)
            if location:
                self.redirect(location)
            else:
                logging.error("Can not detect redirection url")

            return

        title = sub_path.replace('-', ' ').strip()
        table = 'group_credit'
        sql = """
            SELECT text
              FROM {table}, 
                 (SELECT RAND() * (SELECT MAX(id)-10 FROM {table}) AS tid) AS tmp
            WHERE {table}.id >= tmp.tid
            ORDER BY id ASC
            LIMIT 10;
        """.format(table=table)

        cursor.execute(sql)
        records = cursor.fetchall()

        result = template.render("index.html", {"title": title, "records": records})
        self.response.write(result)


app = webapp2.WSGIApplication(
    [
        ('/', IndexPage),
        ('/initial', InitialDB),
        ('/(.*)', SubpathHandler),
    ]
)
