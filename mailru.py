#-*- coding: utf-8 -*-
import requests
import json
import string
import urllib
import sys
import os
import random
import time
import lxml.html
import logging
import re

logging.basicConfig(format=u'# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.DEBUG,
                    filename=os.path.join(os.path.dirname(
                        os.path.abspath(__file__)), 'quora.log'))
logging.getLogger().addHandler(logging.StreamHandler())


class Mailru:
    """
        -*- Mailru parser -*-
        © Copyright by Fil Andrey (python36@yandex.ru, python36@ex.ua)
    """
    def __init__(self, q, g,
                 number_questions=25,
                 number_answers=25,
                 number_moreanswers=15,
                 number_similar=5,
                 similar_level=1,
                 error_sleep=1):
        self.session = requests.Session()
        self.search_string = q
        self.group = g.encode('utf-8')
        self.number_questions = number_questions
        self.number_answers = number_answers
        self.number_moreanswers = number_moreanswers
        self.number_similar = number_similar
        self.similar_level = similar_level
        self.error_sleep = error_sleep
        logging.info('. Start parse: q = "%s", group = "%s"' % (q, g))
        self.main_questions_counter = 0
        self.main_answers_counter = 0
        self.random_bits = map(lambda x: bin(x)[2:].rjust(4, '0'), range(8, 16))
        self.rndm_bits = map(lambda x: bin(x)[2:].rjust(4, '0'), range(32, 64))
        self.server_url = 'https://selanidb.appspot.com/'
        #self.server_url = 'http://127.0.0.1:8080/'
        self.headers = {'Accept': '*/*',
                        'Accept-Encoding': 'gzip, deflate, sdch',
                        'Accept-Language': 'en-US,en;q=0.8',
                        'Connection': 'keep-alive',
                        'CSP': 'active',
                        'Host': 'go.mail.ru',
                        'Referer': ('https://touch.otvet.mail.ru/search'
                                    '/%s') % urllib.quote(self.search_string),
                        'User-Agent': ('Mozilla/5.0 (iPad;U;CPU OS 5_1_1 like '
                                       'Mac OS X; zh-cn)AppleWebKit/534.46.0'
                                       '(KHTML, like Gecko)CriOS/19.0.1084.60 '
                                       'Mobile/9B206 Safari/7534.48.3')}
        self.headers_answer = {'Accept': ('application/json, text/javascript, '
                                          '*/*; q=0.01'),
                               'Accept-Encoding': 'gzip, deflate',
                               'Accept-Language': 'en-US,en;q=0.8',
                               'Connection': 'keep-alive',
                               'Content-Type': ('application/x-www-form-'
                                                'urlencoded; charset=UTF-8'),
                               'Cookie': ('i=AQBMsuFVAQDcBAgEAQEAAQ==; '
                                          'p=/kgAAGS/pwAA; '
                                          'mrcu=17F055E2CEF21D198DA3D327C92E; '
                                          'b=KkEAAHD4AA8AOAAAIITSjMIA; '
                                          'c=3BDrVQEAAJ8KAAAUAAAACQAQ; '
                                          '__utma=11369322.1317534443.1'
                                          '440927475.1440927475.1441472536.2; '
                                          '__utmc=11369322; '
                                          '__utmz=11369322.1440927475.1.1.'
                                          'utmcsr=(direct)|utmccn=(direct)|'
                                          'utmcmd=(none); '
                                          'VID=1Fkp2V0yQQnN0000020414HN::'
                                          '123585889; __utma=169299236.'
                                          '396684502.1440949890.1441468637.'
                                          '1441472492.5; '
                                          '__utmb=169299236.0.10.1441472492; '
                                          '__utmc=169299236; '
                                          '__utmz=169299236.1440949890.1.1.'
                                          'utmcsr=(direct)|utmccn=(direct)|'
                                          'utmcmd=(none)'),
                               'CSP': 'active',
                               'Host': 'touch.otvet.mail.ru',
                               'Origin': 'https://touch.otvet.mail.ru',
                               'User-Agent': ('Mozilla/5.0 (iPad;U;CPU OS 5_1_1'
                                              ' like Mac OS X; zh-cn)'
                                              'AppleWebKit/534.46.0'
                                              '(KHTML, like Gecko)CriOS/19.0.'
                                              '1084.60 Mobile/9B206 '
                                              'Safari/7534.48.3'),
                               'X-Requested-With': 'XMLHttpRequest'}

    def post_to_server(self, text, group=None):
        for s in map(lambda s: s.decode('utf-8'), text):
            logging.debug(s)
        requests.post(url=self.server_url,
                      data={'text': json.dumps(list(text)),
                            'group': group if group is not None else self.group,
                            'token': self.gen_token(sum(
                                map(lambda x: len(x.decode('utf-8')), text)),
                            self.random_bits)})

    def gen_token(self, text, random_bits):
        return hex(int(random.choice(random_bits) +
                   bin(text)[2:] + random.choice(random_bits), 2))[2:]

    def gen_token_initial(self, text):
        return self.gen_token(sum(map(ord, text)), self.rndm_bits)

    f_start_upper = lambda self, s: string.upper(s[0]) + s[1:]

    def f_split(self, text, sep='.!?', link_end=string.ascii_letters+'+?._%/'):
        strings, _tmp = [], ''
        for char in text + '.':
            if char in sep:
                _tmp = re.sub(r"\s?https?:\/\/\S+", "", _tmp)
                if _tmp and len(_tmp.split(' ', 3)) > 3:
                    strings.append(self.f_start_upper(_tmp.lstrip() + char))
                _tmp = ''
            else:
                _tmp += char
        return strings

    def get_add_answers(self, page, question_list, result):
        if result['status'] != '200' and 'error' in result:
            logging.debug('%s - %s' % (result['status'], result['error']))
            return False
        self.main_questions_counter += 1
        for answer in result['answers']:
            self.main_answers_counter += 1
            question_list |= set(self.f_split(
                lxml.html.fromstring(answer['atext']).text_content()
                .encode('utf-8')))

    def question_urls(self, result):
        return [(answer['url'].split('/')[-2],
                 lxml.html.fromstring(answer['question'])
                 .text_content().encode('utf-8'))
                for answer in result['results']]

    def post_get(self, method, url, headers, params={}, data={}):
        while True:
            try:
                if method == 'get':
                    return self.session.get(url=url, params=params,
                                            headers=headers)
                return self.session.post(url=url, data=data, headers=headers)
            except requests.exceptions.ConnectionError:
                logging.debug('# Connection error: sleep %d' % self.error_sleep)
                time.sleep(self.error_sleep)

    def parse(self, level=0):
        requests.post(url=self.server_url + 'initial',
                      data={'group': self.group,
                            'token': self.gen_token_initial(self.group)})
        _t = var_t = int(time.time() * 1000)
        question_urls = []
        sf, count = 0, 1
        while sf < count:
            var_t += 1
            page = self.post_get(method='get',
                                 url="https://go.mail.ru/answer_json",
                                 params={'callback': ('jQuery111101988491497'
                                                      '4682033_%d') % _t,
                                         'q': self.search_string,
                                         'num': self.number_questions,
                                         'sf': sf,
                                         '_': var_t},
                                 headers=self.headers)
            sf += self.number_questions
            result = json.loads(page.text.encode('utf-8')[110:-2])
            count = result['count']
            question_urls += self.question_urls(result)
        var_t += 3
        logging.debug(len(question_urls))
        self.parsing_questions(var_t, question_urls, result)
        logging.debug(('. Finish. questions - %d, '
                       'answers - %d') % (self.main_questions_counter,
                                          self.main_answers_counter))

    def parsing_questions(self, var_t, urls, result, level=0):
        _t = var_t - 1
        for url, text in urls:
            question_list = set(self.f_split(text))
            self.headers_answer['Referer'] = ('https://touch.otvet.mail.ru/'
                                              'question/%s') % url
            logging.info('# Get %s' % url)
            page = self.post_get(method='post',
                                 url=('https://touch.otvet.mail.ru/api/v2/'
                                      'question'),
                                 data={'qid': int(url),
                                       'n': self.number_answers},
                                 headers=self.headers_answer)
            result = json.loads(page.text.encode('utf-8'))
            if self.get_add_answers(page, question_list, result) is False:
                continue
            # moreanswer
            if int(result['anscnt']) > self.number_answers:
                for p in xrange(self.number_answers + 1,
                                int(result['anscnt']) + 1,
                                self.number_moreanswers):
                    page = self.post_get(method='post',
                                         url=('https://touch.otvet.mail.ru/'
                                              'api/v2/moreanswers'),
                                         data={'qid': int(url),
                                               'n': self.number_moreanswers,
                                               'p': p},
                                         headers=self.headers_answer)
                    self.get_add_answers(page, question_list)
            #post to server
            self.post_to_server(question_list)
            # similar
            if level >= self.similar_level:
                continue
            page = self.post_get(method='get',
                                 url="https://go.mail.ru/answer_reco_json?",
                                 params={'callback': ('jQuery11110198849149'
                                                      '74682033_%d') % _t,
                                         'q': url,
                                         'text': text,
                                         'num': self.number_similar,
                                         'sf': 0,
                                         '_': var_t},
                                 headers=self.headers)
            result = json.loads(page.text.encode('utf-8')[110:-2])
            if result['count'] > 1:
                logging.info('# Parse similar')
                self.parsing_questions(var_t, self.question_urls(result),
                                       result, level+1)

if __name__ == '__main__':
    if sys.argv[1] in ['--help', '-h', '-?']:
        print """\
-*- Mailru parser -*-
© Copyright by Fil Andrey (python36@yandex.ru, python36@ex.ua)

SYNOPSIS
\tpython mailru.py [-h | --help | -?] [-f | --keyword_file]

COMMAND LINE OPTIONS
\t-h, --help, -?
\t\tPrint this page

\t-f, --keyword_file
\t\tKeyword file. Default - keyword.txt

--------------------------------------------------------------------------------

PARSER
Parameters with default values
\t--number_questions 25 - The number of questions received at request
\t--number_answers 25 - The number of answers received at request
\t--number_moreanswers 15 - The number of questions received at request \
if number answers > number_questions
\t--number_similar 5 - The number of similar questions
\t--similar_level 1 - How many levels similar questions to parse
\t--error_sleep 1 - Bedtime on error (seconds)

Example
python mailru.py --similar_level 3 --keyword_file example.txt --error_sleep 10\
"""
        sys.exit(0)
    if '--keyword_file' in sys.argv or '-f' in sys.argv:
        file_name = sys.argv[sys.argv.index('--keyword_file') + 1]
    else:
        file_name = 'keyword.txt'
    if '--number_questions' in sys.argv:
        number_questions = int(
            sys.argv[sys.argv.index('--number_questions') + 1])
    else:
        number_questions = 25
    if '--number_answers' in sys.argv:
        number_answers = int(sys.argv[sys.argv.index('--number_answers') + 1])
    else:
        number_answers = 25
    if '--number_moreanswers' in sys.argv:
        number_moreanswers = int(
            sys.argv[sys.argv.index('--number_moreanswers') + 1])
    else:
        number_moreanswers = 15
    if '--number_similar' in sys.argv:
        number_similar = int(sys.argv[sys.argv.index('--number_similar') + 1])
    else:
        number_similar = 5
    if '--similar_level' in sys.argv:
        similar_level = int(sys.argv[sys.argv.index('--similar_level') + 1])
    else:
        similar_level = 1
    if '--error_sleep' in sys.argv:
        error_sleep = int(sys.argv[sys.argv.index('--error_sleep') + 1])
    else:
        error_sleep = 1

    while True:
        with open(file_name) as f:
            data = f.readlines()
            if len(data) < 2:
                if len(data) == 0 or not data[0].strip():
                    sys.exit()
                else:
                    data += ['']
            q, g = data[0].split(':')
            if q.strip() and g.strip():
                mailru = Mailru(q[1:-1], g.rstrip(),
                                number_questions=number_questions,
                                number_answers=number_answers,
                                number_moreanswers=number_moreanswers,
                                number_similar=number_similar,
                                similar_level=similar_level,
                                error_sleep=error_sleep)
                mailru.parse()
        with open(file_name, 'w') as f:
            f.writelines(data[1:])
