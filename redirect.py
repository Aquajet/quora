# -*- coding: utf-8 -*-
#!/usr/bin/env python
# coding: utf-8

import os
import webapp2

from uuid import uuid4
from random import choice
from urllib import urlencode
from datetime import datetime, timedelta

from google.appengine.ext.webapp import template
from google.appengine.api import urlfetch, memcache
import ganalytics

from logic import where_to_go


class MainPage(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        self.response.write(template.render("index.html", {}))


class MainHandler(webapp2.RequestHandler):

    def get(self):
        user_agent = self.request.headers.get("User-Agent")

        try:
            cid = self.request.cookies["cidic"]
        except KeyError:
            cid = str(uuid4())
            self.response.set_cookie(key='cidic', value=cid, expires=datetime.now() + timedelta(days=2*365), path='/')

        GoogleAnalyticsClass(self, self.request.path)

        if "googlebot" in user_agent.lower():
            filename = os.path.basename(self.request.path)
            file_html = self.find_file(os.path.join("mods", filename[0].lower()), filename.lower())

            try:
                self.response.write(template.render(file_html, {}))
            except Exception as error:
                self.abort(404)

            return

        location = where_to_go(self.request)
        if location:
            self.redirect(location)

    def find_file(self, directory, file_to_find):
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.lower() == file_to_find:
                    return os.path.join(root, file)


app = webapp2.WSGIApplication([('/', MainPage),
                               (r'.*?', MainHandler),
                              ], debug=True)
